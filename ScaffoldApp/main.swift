import Foundation
import Alamofire
import libwebp

func useVulnerableFunction() {
    // Example of using the vulnerable ReadHuffmanCodes() function in libwebp 0.6.0
    var config = WebPDecoderConfig()
    WebPInitDecoderConfig(&config)

    let data: [UInt8] = [0x52, 0x49, 0x46, 0x46, 0x3A, 0x00, 0x00, 0x00, 0x57, 0x45, 0x42, 0x50, 0x56, 0x50, 0x38, 0x4C]
    data.withUnsafeBytes {
        WebPGetFeatures($0.baseAddress?.assumingMemoryBound(to: UInt8.self), data.count, &config.input)
    }

    // This is the vulnerable function call
    let status = WebPDecode(nil, 0, &config)
    if status != VP8_STATUS_OK {
        print("Decoding failed with status: \(status)")
    } else {
        print("Decoding succeeded")
    }
}

func main() {
    print("Starting application...")

    // Safe usage of Alamofire
    let request = AF.request("https://httpbin.org/get")
    request.response { response in
        debugPrint(response)
    }

    // Vulnerable usage of libwebp
    useVulnerableFunction()

    print("Application finished.")
}

main()
